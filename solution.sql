-- Find all artists that has letter 'd' in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- Fill all songs that has a lenth of less than 230
SELECT * FROM songs WHERE length < 230;

-- Join the album and songs tables. (Only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums 
    JOIN songs on albums.id = songs.album_id;

-- Join the artists and albums tables. (Find all albums that has letter 'a' in its name)
SELECT * FROM albums
    JOIN artists ON albums.artist_id = artists.id
    WHERE albums.album_title LIKE "%a%";

-- Sort the albums in Z-A orders (Show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs tables(Sort albums from Z-A)
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC;